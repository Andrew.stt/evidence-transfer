export default {
    title: "Склад полиции",
    subtitle: "Сдача улик",
    empty: "Нет улик для сдачи",
    weight: 'кг',
    count: 'ед'
}